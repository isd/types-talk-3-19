{-# LANGUAGE OverloadedStrings #-}
module Main where

import Prelude hiding (pi)

import Data.Foldable (for_, traverse_)
import Data.String   (fromString)

import Data.ByteString.Builder       (hPutBuilder)
import System.IO                     (IOMode(WriteMode), withFile)
import Text.Blaze.Html               (Html, (!))
import Text.Blaze.Html.Renderer.Utf8 (renderHtmlBuilder)

import qualified Text.Blaze.Html5            as H
import qualified Text.Blaze.Html5.Attributes as A


page :: Html -> Html
page content = H.docTypeHtml $ do
    H.head $ do
        H.meta ! A.charset "utf-8"
        H.title "A Whirlwind Tour of Type Systems"
        H.link ! A.rel "stylesheet" ! A.href "style.css"
        H.script ! A.src "keyhandling.js" $ pure ()
    H.body
        content

slide :: Html -> Html -> Html
slide title content = H.section $ do
    H.h1 (H.toHtml title)
    content

addSpan :: Html -> Html
addSpan e = H.span ! A.class_ "add" $ e

addBlock :: Html -> Html
addBlock e = H.div ! A.class_ "add" $ e

delSpan :: Html -> Html
delSpan e = H.span ! A.class_ "del" $ e

delBlock :: Html -> Html
delBlock e = H.div ! A.class_ "del" $ e

cpSmallLam, cpSmallOmega, cpBigLam, cpBigOmega, cpRightArrow, cpFatArrow :: Char
cpSmallLam = toEnum 0x3bb
cpSmallOmega = toEnum 0x3c9
cpBigLam = toEnum 0x39b
cpBigOmega = toEnum 0x3a9
cpRightArrow = toEnum 0x2192
cpFatArrow = toEnum 0x21d2

cpForall :: Char
cpForall = toEnum 0x2200

cpBigPi = toEnum 0x3a0 :: Char

fw :: Html
fw = "F" >> H.sub (H.toHtml [cpSmallOmega])

fc :: Html
fc = "F" >> H.sub "c"

(-->) :: Html -> Html -> Html
l --> r = l >> fromString [' ', cpRightArrow, ' '] >> r

(==>) :: Html -> Html -> Html
l ==> r = l >> fromString [' ', cpFatArrow , ' '] >> r

var :: Html -> Html
var = id

binder :: Char -> Html -> Html -> Html
binder c param body = do
    H.toHtml [c]
    var param
    "."
    body

lam :: Html -> Html -> Html
lam = binder cpSmallLam

tyLam :: Html -> Html -> Html
tyLam = binder cpBigLam

forall_ :: Html -> Html -> Html
forall_ = binder cpForall

pi :: Html -> Html -> Html
pi = binder cpBigPi

app :: Html -> Html -> Html
app f x = f >> " " >> x

tyApp f t = app f ("[" >> t >> "]")

parens :: Html -> Html
parens x = "(" >> x >> ")"

syntax :: String -> [Html] -> Html
syntax name cases = do
    H.toHtml name
    " ::= "
    H.ul ! A.class_ "syntax" $
        traverse_ H.li cases

progression :: Html -> [Html] -> Html
progression title parts =
    for_ [1..length parts] $ \i -> do
        slide title $ sequence_ (take i parts)

presentation = page $ do
    progression "A Whirlwind Tour of Type Systems"
        [ pure ()
        , do
            H.h2 "Whence This Talk"
            H.p "Two weeks ago:"
            H.ul $ do
                H.li $ H.toHtml $
                    "Me: Something something System " >> fw >> "."
                H.li $ H.toHtml $ do
                    "Sam: It would be neat if someone gave a talk "
                    "explaining what System " >> fw
                    " and all those things are."
        ]
    slide "Road Map" $ do
        H.ul $ do
            H.li "Untyped Lambda Calculus"
            H.li $ do
                "Important Type Systems"
                H.ul $ do
                    H.li "Simply Typed Lambda Calculus"
                    H.li "System F"
                    H.li "Hindley-Milner (very briefly)"
                    H.li $ H.toHtml $ "System " >> fw
            H.li "Appetizers for Further Topics"
        H.p $ "Omission: Subtyping"
    slide "Untyped Lambda Calculus" $ do
        syntax "t"
            [ var "x"
            , lam "x" "t"
            , app "t" "t"
            ]
    progression "Turing Complete"
        [ pure ()
        , H.p $ do
            fromString (cpBigOmega : " = ")
            app
                (parens (lam "x" (app (var "x") (var "x"))))
                (parens (lam "x" (app (var "x") (var "x"))))
        , H.p $ do
            "fix = "
            lam "f" $ app
                (parens $ lam "x" $ app (var "f") $ parens (app (var "x") (var "x")))
                (parens $ lam "x" $ app (var "f") $ parens (app (var "x") (var "x")))
        , H.pre $ H.toHtml $ unlines $ map concat
            [ ["fix (", [cpSmallLam], "factorial. ", [cpSmallLam], "n."]
            , ["  if n == 0 then"]
            , ["    1"]
            , ["  else"]
            , ["    n * factorial (n - 1))"]
            ]
        ]
    slide "Let's Add Types!" $
        syntax "t"
            [ var "x"
            , lam "x" "t"
            , app "t" "t"
            ]
    slide "Let's Add Types!" $ do
        syntax "t"
            [ var "x"
            , lam ("x" >> addSpan ":T") "t"
            , app "t" "t"
            ]
        addBlock $ syntax "T"
            [ "T" --> "T"
            ]
    slide "Take 2: Simply Typed Lambda Calculus" $ do
        syntax "t"
            [ var "x"
            , lam "x:T" "t"
            , app "t" "t"
            , addSpan "unit"
            ]
        syntax "T"
            [ "T" --> "T"
            , addSpan "Unit"
            ]
    slide "System F: Adding Polymorphism" $ do
        syntax "t"
            [ var "x"
            , lam "x:T" "t"
            , app "t" "t"
            , addSpan $ tyLam "X" "t"
            , addSpan $ tyApp "t" "T"
            , delSpan "unit"
            ]
        syntax "T"
            [ "T" --> "T"
            , addSpan $ "X"
            , addSpan $ forall_ "X" "T"
            , delSpan "Unit"
            ]
        H.h2 "Relevant Haskell Extensions"
        H.ul $ do
            H.li "RankNTypes"
            H.li "TypeApplications"
    progression "System F: Example"
        [ H.pre $ do
            "id : " >> forall_ "X" ("X" --> "X") >> "\n"
            "   = " >> tyLam "X" (lam "x:X" "x") >> "\n"
        , H.pre $ do
            tyApp "id" "Bool" >> " : " >> ("Bool" --> "Bool") >> "\n"
            "   = " >> lam "x:Bool" "x" >> "\n"
        , H.pre $ do
            app (tyApp "id" "Bool") "true" >> " : Bool\n"
            "   = " >> app ("(" >> lam "x:Bool" "x" >> ")") "true" >> "\n"
            "   = true" >> "\n"
        ]
    progression "System F"
        [ H.p "Still not turing complete"
        , H.pre $ "fix : " >> forall_ "X" ("(" >> ("X" --> "X") >> ")") --> "X"
        , H.p "...but the body doesn't type check."
        ]
    progression "Hindley-Milner: \"Let-Polymorphism\""
        [ pure ()
        , H.pre $ H.toHtml $ unlines
            [ "# let id x = x;;"
            , "val id : 'a -> 'a = <fun>"
            ]
        , H.p $ "Implicitly: " >> forall_ "a" ("a" --> "a")
        , do
            H.p "Can't do "
            "runST : " >>
                (forall_ "a" $
                    ("(" >> (forall_ "s" (tyApp (tyApp "ST" "s") "a")) >> ")") --> "a"
                )
        , H.p "...but global type inference becomes possible."
        ]
    progression ("System " >> fw >> ": System F + Kinds")
        [ pure ()
        , do
            syntax "t"
                [ var "x"
                , lam "x:T" "t"
                , app "t" "t"
                , delSpan $ tyLam "X" "t"
                , addSpan $ lam ("X" >> addSpan "::K") "t"
                , tyApp "t" "T"
                ]
            syntax "T"
                [ "T" --> "T"
                , "X"
                , forall_ ("X" >> addSpan "::K") "T"
                , addSpan $ lam "X::K" "T"
                , addSpan $ app "T" "T"
                ]
            addBlock $ syntax "K"
                [ "K" ==> "K"
                , "*"
                ]
        ]
    slide "Remember Simple Types?" $
        syntax "T"
            [ "T" --> "T"
            , "Unit"
            ]
    slide ("System " >> fw >> ": Scrap Your Type Classes") $
        H.pre $ H.toHtml $ unlines
            [ "{-# LANGUAGE RankNTypes     #-}"
            , "{-# LANGUAGE KindSignatures #-}"
            , ""
            , "data Functor' (f :: * -> *) = Functor'"
            , "    { fmap' :: forall a b. (a -> b) -> f a -> f b"
            , "    }"
            ]
    progression "Appetizers"
        [ H.ul $ H.li $ "System " >> fc >> ": " >> fw >> " + Type Equalities."
        , H.ul $ H.li $ "Lots of relatively self-contained extensions:" >> (H.ul $ do
            H.li "Tuples"
            H.li "Records"
            H.li "Variants/Unions"
            H.li "etc")
        , H.ul $ H.li "Recursive Types"
        , H.ul $ H.li "Subtyping"
        , H.ul $ H.li $ "Dependent Types: " >> pi "x:T" "T"
        , H.ul $ H.li "Regions (\"lifetimes\" in Rust)"
        , H.ul $ H.li "Substructural Types: Move Semantics & More"
        ]
    slide "Learning More" $ do
        H.img ! A.src "https://www.cis.upenn.edu/~bcpierce/tapl/taplcover.jpg"

main :: IO ()
main = withFile "out.html" WriteMode $ \h ->
    hPutBuilder h (renderHtmlBuilder presentation)
