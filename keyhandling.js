document.addEventListener('DOMContentLoaded', function() {
	var elts = document.getElementsByTagName('section');
	var currentSlide = 0;
	elts[currentSlide].classList = ['current'];
	var updateSlide = function(diff) {
		elts[currentSlide].classList = [];
		currentSlide += diff;
		elts[currentSlide].classList = ['current'];
	}
	document.addEventListener('keydown', function(e) {
		if(e.key === 'ArrowLeft' && currentSlide !== 0) {
			updateSlide(-1);
		} else if (e.key === 'ArrowRight' && currentSlide !== elts.length - 1) {
			updateSlide(1);
		}
	})
})
